<?php
/**
 * Copyright (C) ATHOS TRADER SL <info@athostrader.com>
 */
namespace FacturaScripts\Plugins\WebMembers\Lib\Shortcode;

use FacturaScripts\Dinamic\Lib\Shortcode\Shortcode;
use FacturaScripts\Dinamic\Model\Member;
use FacturaScripts\Core\Base\DataBase\DataBaseWhere;
use FacturaScripts\Dinamic\Model\AttachedFile;
use FacturaScripts\Dinamic\Lib\AssetManager;

/**
 * Shortcode of webMember
 * Shows the members of one or more categories
 *
 * @author Athos Online <info@athosonline.com>
 */
class webMember extends Shortcode
{
    public static function addAsset()
    {
        if (static::getClassName(get_class())) {
            AssetManager::add('css', FS_ROUTE . '/Dinamic/Assets/CSS/webMember.css');
            AssetManager::add('js', FS_ROUTE . '/Dinamic/Assets/JS/webMember.js');;
        }
    }

    /**
     * Replace the block shortcode with the content of the block if found
     * 
     * @param string $content
     *
     * @return string
     */
    public static function replace($content)
    {
        $shorts = static::searchCode($content, "/\[webMember(.*?)\]/");
        
        if (count($shorts[0]) <= 0) {
            return $content;
        }
        
        $members = new member();
        $where = [new DataBaseWhere('type', 'Member')];
        $appSettings = static::toolBox()->appSettings();
        $permitted_chars = 'abcdefghijklmnopqrstuvwxyz';
        $addCode = false;

        for ($x = 0; $x < count($shorts[1]); $x++) {
            $params = static::getAttributes($shorts[1][$x]);
            
            $type = isset($params['type']) ? $params['type'] : 'columns';
            $categories = isset($params['categories']) ? explode('|', $params['categories']) : null;
            $class = isset($params['class']) ? $params['class'] : '';
            $id = isset($params['id']) ? $params['id'] : substr(str_shuffle($permitted_chars), 0, 10);
            $name = (isset($params['name']) && $params['name'] == 'yes') ? true : false;
            $link = (isset($params['link']) && $params['link'] == 'yes') ? true : false;
            $description = (isset($params['description']) && $params['description'] == 'yes') ? true : false;
            $image = (isset($params['image']) && $params['image'] == 'yes') ? true : false;
            $interval = isset($params['interval']) ? $params['interval'] : '3000';

            switch ($type) {
                case 'columns':
                    $html = self::viewColumns($members, $where, $appSettings, $categories, $class, $id, $name, $link, $description, $image);
                    break;
                
                case 'slides':
                    $html = self::viewSlides($members, $where, $appSettings, $categories, $class, $id, $name, $link, $description, $image, $interval);
                    break;    
            }

            if (isset($html) && !empty($html)) {
                $content = str_replace($shorts[0][$x], $html, $content);
                $addCode = true;
            }
        }

        if ($addCode) {
            self::addAsset();
        }

        return $content;
    }

    private static function viewSlides($members, $where, $appSettings, $categories, $class, $id, $name, $link, $description, $image, $interval)
    {
        $members->clear();
        $html = '';

        if ($members->count($where) > 0) {
            $html = '<div id="'.$id.'" class="carousel slide w-100 '.$class.'" data-ride="carousel" data-interval="'.$interval.'">';
            $html .= '<div class="carousel-inner w-100" role="listbox">';

            $index = 0;
            foreach($members->all($where, [], 0, 0) as $member) {
                if (in_array($member->idcategory, $categories)) {
                    $active = '';
                    if ($index === 0) {
                        $active = 'active';
                    }

                    $html .= '<div class="carousel-item '.$active.'">';
                    $html .= '<div class="col-md-4">';
                    $html .= '<div class="card card-body">';

                    if ($link) {
                        $url = $appSettings->get('webcreator', 'siteurl').$member->permalink;
                        $html .= '<a class="text-decoration-none" href="'.$url.'" class="member-link">';
                    }

                    if ($image && !is_null($member->idfile)) {
                        $file = new AttachedFile();
                        $file->loadFromCode($member->idfile);
                        $imgMember = $file->url('download-permanent');

                        $html .= '<div class="member-image">';
                        $html .= '<img src="'.$imgMember.'" class="img-fluid">';
                        $html .= '</div>';
                    }

                    if ($name) {
                        $html .= '<div class="member-name">';
                        $html .= $member->title;
                        $html .= '</div>';
                    }

                    if ($description) {
                        $html .= '<div class="member-description">';
                        $html .= $member->description;
                        $html .= '</div>';
                    }

                    if ($link) {
                        $html .= '</a>';
                    }

                    $html .= '</div>';
                    $html .= '</div>';
                    $html .= '</div>';
                    $index++;
                }
            }

            $html .= '</div>';

            $html .= '
                <a class="carousel-control-prev w-auto" href="#'.$id.'" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon bg-dark border border-dark rounded-circle" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next w-auto" href="#'.$id.'" role="button" data-slide="next">
                    <span class="carousel-control-next-icon bg-dark border border-dark rounded-circle" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>';

            $html .= '</div>';
        }

        return $html;
    }

    private static function viewColumns($members, $where, $appSettings, $categories, $class, $id, $name, $link, $description, $image)
    {
        $members->clear();
        $html = '';

        if ($members->count($where) > 0) {
            $html = '<div id="'.$id.'" class="row text-center '.$class.'">';
            
            foreach($members->all($where, [], 0, 0) as $member) {
                if (in_array($member->idcategory, $categories)) {
                    $html .= '<div class="member-item col-12 col-md-3 col-lg-4">';

                    if ($link) {
                        $url = $appSettings->get('webcreator', 'siteurl').$member->permalink;
                        $html .= '<a class="text-decoration-none" href="'.$url.'" class="member-link">';
                    }

                    if ($image && !is_null($member->idfile)) {
                        $file = new AttachedFile();
                        $file->loadFromCode($member->idfile);
                        $imgMember = $file->url('download-permanent');

                        $html .= '<div class="member-image">';
                        $html .= '<img src="'.$imgMember.'" style="max-width: 100%; height: auto;">';
                        $html .= '</div>';
                    }

                    if ($name) {
                        $html .= '<div class="member-name">';
                        $html .= $member->title;
                        $html .= '</div>';
                    }

                    if ($description) {
                        $html .= '<div class="member-description">';
                        $html .= $member->description;
                        $html .= '</div>';
                    }

                    if ($link) {
                        $html .= '</a>';
                    }

                    $html .= '</div>';
                }
            }

            $html .= '</div>';
        }

        return $html;
    }
}