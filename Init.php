<?php
/**
 * Copyright (C) ATHOS TRADER SL <info@athostrader.com>
 */
namespace FacturaScripts\Plugins\WebMembers;

use FacturaScripts\Core\Base\InitClass;
use FacturaScripts\Plugins\WebCreator\Lib\Shortcode\Shortcode;
use FacturaScripts\Dinamic\Model\Settings;

/**
 * Description of Init
 *
 * @author Athos Online <info@athosonline.com>
 */
class Init extends InitClass
{

    public function init()
    {
        if (WEBMULTILANGUAGE) {
            $this->addSettings(['permalink_member']);
        }

        $this->loadExtension(new Extension\Controller\WebCreator());
        Shortcode::addCode('member', 'webMember');
    }

    public function update()
    {
        if (WEBMULTILANGUAGE) {
            $this->addSettings(['permalink_member']);
            $modelTranslate = '\\FacturaScripts\\Dinamic\\Lib\\Portal\\TranslatePlugins';
            $modelTranslate::translateSettings();
        }

        $appSettings = $this->toolBox()->appSettings();
        if (empty($appSettings->get('webcreator', 'permalink_member'))) {
            $appSettings->set('webcreator', 'permalink_member', 'miembro');
        }

        $appSettings->save();
    }

    public function addSettings($values)
    {
        foreach ($values as $value) {
            if (!in_array($value, Settings::$fieldsTranslate)) {
                array_push(Settings::$fieldsTranslate, $value);
            }
        }
    }
}