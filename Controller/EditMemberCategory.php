<?php
/**
 * Copyright (C) ATHOS TRADER SL <info@athosonline.com>
 */
namespace FacturaScripts\Plugins\WebMembers\Controller;

use FacturaScripts\Dinamic\Lib\ExtendedController\PanelController;

/**
 * Description of EditMemberCategory
 *
 * @author Athos Online <info@athosonline.com>
 */
class EditMemberCategory extends PanelController
{
    /**
     * Returns basic page attributes
     *
     * @return array
     */
    public function getPageData()
    {
        $pagedata = parent::getPageData();
        $pagedata['menu'] = 'web';
        $pagedata['title'] = 'category';
        $pagedata['icon'] = 'fas fa-list-alt';
        $pagedata['showonmenu'] = false;
        return $pagedata;
    }

    /**
     * Load views.
     */
    protected function createViews()
    {
        $this->addEditView('EditMemberCategory', 'MemberCategory', 'category', 'fas fa-list-alt');
    }

    protected function loadData($viewName, $view) {
        switch ($viewName) {
            case 'EditMemberCategory':
                $code = $this->request->get('code');
                $view->loadData($code);
                break;
        }
    }
}