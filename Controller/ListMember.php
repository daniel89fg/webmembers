<?php
/**
 * Copyright (C) ATHOS TRADER SL <info@athosonline.com>
 */
namespace FacturaScripts\Plugins\WebMembers\Controller;

use FacturaScripts\Core\Lib\ExtendedController;
use FacturaScripts\Core\Base\DataBase\DataBaseWhere;

/**
 * Description of ListMember
 *
 * @author Athos Online <info@athosonline.com>
 */
class ListMember extends ExtendedController\ListController
{
    /**
     * Returns basic page attributes
     *
     * @return array
     */
    public function getPageData()
    {
        $data = parent::getPageData();
        $data['menu'] = 'web';
        $data['title'] = 'members';
        $data['icon'] = 'fas fa-users';
        return $data;
    }
    
    protected function createViews() {
        $this->createViewsMembers();
        $this->createViewsCategories();
    }
    
    /**
     * 
     * @param string $viewName
     */
    protected function createViewsMembers(string $viewName = 'ListMember')
    {
        $this->addView($viewName, 'Member', 'members', 'fas fa-users');
    }
    
    /**
     * 
     * @param string $viewName
     */
    protected function createViewsCategories(string $viewName = 'ListMemberCategory')
    {
        $this->addView($viewName, 'MemberCategory', 'categories', 'fas fa-list-alt');
    }

    protected function loadData($viewName, $view) {
        switch ($viewName) {
            case 'ListMember':
                $where = [new DataBaseWhere('type', 'Member')];
                $view->loadData('', $where);
                break;

            default:
                parent::loadData($viewName, $view);
                break;
        }
    }
}