<?php
/**
 * Copyright (C) ATHOS TRADER SL <info@athosonline.com>
 */
namespace FacturaScripts\Plugins\WebMembers\Controller;

use FacturaScripts\Dinamic\Lib\ExtendedController\PanelController;
use FacturaScripts\Dinamic\Lib\AssetManager;
use FacturaScripts\Dinamic\Model\Member;
use FacturaScripts\Dinamic\Model\MemberCategory;
use FacturaScripts\Dinamic\Model\WebHeader;
use FacturaScripts\Dinamic\Model\WebSidebar;
use FacturaScripts\Dinamic\Model\WebFooter;
use FacturaScripts\Core\Base\DataBase\DataBaseWhere;

/**
 * Description of EditMember
 *
 * @author Athos Online <info@athosonline.com>
 */
class EditMember extends PanelController
{

    /**
     * Returns basic page attributes
     *
     * @return array
     */
    public function getPageData()
    {
        $pagedata = parent::getPageData();
        $pagedata['menu'] = 'web';
        $pagedata['title'] = 'member';
        $pagedata['icon'] = 'fas fa-user';
        $pagedata['showonmenu'] = false;
        return $pagedata;
    }

    /**
     * Load views.
     */
    protected function createViews()
    {
        $this->addHtmlView('EditMember', 'Web/Admin/EditMember', 'Member', 'member', 'fas fa-user');
    
        if (WEBMULTILANGUAGE) {
            $this->addEditListView('EditMemberTranslate', 'WebTranslate', 'translations', 'fa fa-language');
            $this->setTabsPosition('top');
        }
    }

    protected function loadData($viewName, $view) {
        switch ($viewName) {
            case 'EditMember':
                AssetManager::add('css', FS_ROUTE . '/Dinamic/Assets/CSS/codemirror.css');
                AssetManager::add('js', FS_ROUTE . '/Dinamic/Assets/JS/codemirrorBundle.js');
                $code = $this->request->get('code');
                $view->loadData($code);
                break;

            case 'EditMemberTranslate':
                $modelLanguage = '\\FacturaScripts\\Dinamic\\Model\\WebLanguage';
                // Languages
                $columnLanguages = $this->views['EditMemberTranslate']->columnForName('language');
                if ($columnLanguages && $columnLanguages->widget->getType() === 'select') {
                    $customValues = [];
                    foreach ($modelLanguage::getWebLanguages() as $lang) {
                        $customValues[] = [
                            'value' => $lang->codicu,
                            'title' => $lang->name
                        ];
                    }
                    $columnLanguages->widget->setValuesFromArray($customValues);
                }

                // Keys Fields translates
                $columnKeys = $this->views['EditMemberTranslate']->columnForName('key');
                if ($columnKeys && $columnKeys->widget->getType() === 'select') {
                    $customValues = [];
                    foreach (Member::$fieldsTranslate as $field) {
                        $customValues[] = [
                            'value' => $field,
                            'title' => $field
                        ];
                    }
                    $columnKeys->widget->setValuesFromArray($customValues);
                }

                $where = [
                    new DataBaseWhere('modelid', $this->request->get('code')),
                    new DataBaseWhere('modelname', 'Member')
                ];
                $view->loadData('', $where);
                break;
        }
    }

    /**
     * Run the actions that alter data before reading it.
     *
     * @param string $action
     *
     * @return bool
     */
    protected function execPreviousAction($action)
    {
        $activetab = $this->request->request->get('activetab');
        switch ($activetab) {
            case 'EditMember':
                switch ($action) {
                    case 'edit':
                        $page = new Member();
                        $page->loadFromData($this->request->request->all());

                        if ($page->save()) {
                            $this->toolBox()->i18nLog()->notice('record-updated-correctly');
                            return true;
                        } else {
                            $this->toolBox()->i18nLog()->error('record-save-error');
                            return false;
                        }
                        break;

                    case 'insert':
                        $page = new Member();
                        $page->loadFromData($this->request->request->all());

                        if ($page->save()) {
                            $this->redirect($activetab . '?code='.$page->idpage.'&action=save-ok');
                        }
                        break;

                    case 'delete':
                        $page = new Member();
                        $page->loadFromCode($this->request->request->get('code'));
                        if ($page->delete()) {
                            $this->toolBox()->i18nLog()->notice('record-deleted-correctly');
                            return true;
                        } else {
                            $this->toolBox()->i18nLog()->warning('record-deleted-error');
                            return false;
                        }
                        break;
                }
                break;
        }

        return parent::execPreviousAction($action);
    }

    public function getImages()
    {
        return $this->codeModel->all('attached_files', 'idfile', 'filename', true, [
            new DataBaseWhere('mimetype', 'image/gif,image/jpeg,image/png', 'IN')
        ]);
    }

    public function getSiteUrl()
    {
        return $this->toolBox()->appSettings()->get('webcreator', 'siteurl');
    }

    public function getCategories()
    {
        $page = new MemberCategory();
        return $page->all([], [], 0, 0);
    }

    public function getHeaders()
    {
        $header = new WebHeader();
        return $header->all([], [], 0, 0);
    }

    public function getSidebars()
    {
        $sidebar = new WebSidebar();
        return $sidebar->all([], [], 0, 0);
    }

    public function getFooters()
    {
        $footer = new WebFooter();
        return $footer->all([], [], 0, 0);
    }
}