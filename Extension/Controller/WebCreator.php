<?php

namespace FacturaScripts\Plugins\WebMembers\Extension\Controller;

use FacturaScripts\Dinamic\Model\Member;

class WebCreator
{
    public function createViews() {
        return function() {
            if (WEBMULTILANGUAGE) {
                $this->views['WebSettingsPermalink']->disableColumn('members', true);
            }
        };
    }

    public function execAfterAction() {
        return function($action) {
            $activetab = $this->request->request->get('activetab');
            switch ($activetab) {
                case 'WebSettingsPermalink':
                    Member::refreshPermalinkMembers();
            }
            return true;
        };
    }
}