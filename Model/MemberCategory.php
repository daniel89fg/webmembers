<?php
/**
 * Copyright (C) ATHOS TRADER SL <info@athosonline.com>
 */
namespace FacturaScripts\Plugins\WebMembers\Model;

use FacturaScripts\Core\Model\Base;

/**
 * Description of MemberCategory
 *
 * @author Athos Online <info@athosonline.com>
 */
class MemberCategory extends Base\ModelClass
{
    use Base\ModelTrait;
    
    /**
     *
     * @var serial
     */
    public $idcategory;
    
    /**
     *
     * @var string
     */
    public $name;
    
    public static function primaryColumn()
    {
        return 'idcategory';
    }

    public static function tableName()
    {
        return 'members_categories';
    }
    
    /**
     *
     * @param string $type
     * @param string $list
     *
     * @return string
     */
    public function url(string $type = 'auto', string $list = 'List'): string
    {
        return parent::url($type, 'ListMember?activetab=List');
    }
}