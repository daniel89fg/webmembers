<?php
/**
 * Copyright (C) ATHOS TRADER SL <info@athosonline.com>
 */
namespace FacturaScripts\Plugins\WebMembers\Model;

use FacturaScripts\Dinamic\Model\WebPage;

/**
 * Description of Member
 *
 * @author Athos Online <info@athosonline.com>
 */
class Member extends WebPage
{
    public function url(string $type = 'auto', string $list = 'List')
    {
        switch ($type) {
            case 'Member':
                return parent::url($type, 'ListMember?activetab=List');

            default:
                return parent::url($type, $list);
        }
    }
}